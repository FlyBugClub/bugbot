from transformers import GPT2LMHeadModel, GPT2Tokenizer


def generate_text(question):
    
    tokenizer = GPT2Tokenizer.from_pretrained("gpt2")
    model = GPT2LMHeadModel.from_pretrained("gpt2", pad_token_id=tokenizer.eos_token_id)
    
    sentence = question
    print(sentence)
    input_ids = tokenizer.encode(sentence, return_tensors='pt')
    
    output = model.generate(input_ids, max_length=200, num_beams=5, no_repeat_ngram_size=2, early_stopping=True)
             
     
    answer = tokenizer.decode(output[0], skip_special_tokens=True)
    return(answer)
if __name__ == "__main__":
    print("Nhập câu hỏi")
    question = input()
    answer = generate_text(question)
    print(answer)
    

